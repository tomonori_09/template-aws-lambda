local開発環境でlambdaを試す

```bash
docker compose build
docker compose up -d

curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'

{"statusCode":200,"body":"\"Hello from Lambda!\""}
```